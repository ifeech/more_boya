var log = {
	onLog: 1,

	show : function(text){
		if(this.onLog) console.log(text);
	},

	showFdmap : function(){
		console.log(field.fdmap);
	},

	viewField : function(){
		cells = document.getElementsByClassName('cell');
		for(var i=0; i < cells.length; i++){
			if(field.fdmap[i] == '-1'){
				cells[i].style.backgroundColor = "yellow";
			}
			if(field.fdmap[i] == '1'){
				cells[i].style.backgroundColor = "blue";
			}
			if(field.fdmap[i] == '0'){
				cells[i].style.backgroundColor = "gray";
			}
		}
	},
}

var field = {
	row : 10,
	chooseCells : [],

	init : function(){
		this.sqRow = this.row * this.row;
		this.createDefaultMap();
		this.setEventsCells(document.getElementsByClassName('cell'));
	},

	createDefaultMap : function(){
		this.fdmap = new Array(this.sqRow);
		for(var i=0; i < this.fdmap.length; i++) this.fdmap[i]=0;
	},

	setEventsCells : function(cells){
		for(var i=0; i < cells.length; i++){
			cells[i].onclick = this.setClickCells;
			cells[i].onmouseover = this.setMouseOverCells;	
			cells[i].onmouseout = this.setMouseOutCells;	
		}
	},

	getTypeChooseCells : function(chooseCells){
		if(chooseCells.length > 1){
			if(Math.abs(chooseCells[1] - chooseCells[0]) == this.row) return 'V';
		}
		return 'G';
	},

	getCurrentIdCell : function(el){
		currentIdCell = el.getAttribute('idcell');
		if(currentIdCell < 0 || currentIdCell > this.sqRow){
			currentIdCell = null;
		}
		return currentIdCell;
	},

	saveChooseCells : function(){
		log.show('Корабль --' + this.chooseCells.length + '---');
		for(var i=0; i < this.chooseCells.length; i++){
			this.fdmap[this.chooseCells[i]] = 1;
			log.show('Вид корабля --' + this.chooseCells[i] + '---');

			if(this.getTypeChooseCells(this.chooseCells) == 'G'){
				log.show('Тип -- G');
				if(i == 0){
					log.show('Палуба --Fist');
					this.blockFdmapCells(this.chooseCells[i], - 1);
					this.blockFdmapCells(this.chooseCells[i] - this.row);
					this.blockFdmapCells(this.chooseCells[i] - this.row, - 1);
					this.blockFdmapCells(this.chooseCells[i] + this.row);
					this.blockFdmapCells(this.chooseCells[i] + this.row, - 1);
				}
				if(i == this.chooseCells.length-1){
					log.show('Палуба --Last');
					this.blockFdmapCells(this.chooseCells[i], 1);
					this.blockFdmapCells(this.chooseCells[i] - this.row);
					this.blockFdmapCells(this.chooseCells[i] - this.row, 1);
					this.blockFdmapCells(this.chooseCells[i] + this.row);
					this.blockFdmapCells(this.chooseCells[i] + this.row, 1);
				}
				if(i != this.chooseCells.length-1 && i != 0){
					log.show('Палуба --Middle');
					this.blockFdmapCells(this.chooseCells[i] - this.row);
					this.blockFdmapCells(this.chooseCells[i] + this.row);
				}
			}
			if(this.getTypeChooseCells(this.chooseCells) == 'V'){
				log.show('Тип -- V');
				if(i == 0){
					log.show('Палуба --Fist');
					this.blockFdmapCells(this.chooseCells[i] - this.row);
					this.blockFdmapCells(this.chooseCells[i] - this.row, - 1);
					this.blockFdmapCells(this.chooseCells[i] - this.row, 1);
					this.blockFdmapCells(this.chooseCells[i], - 1);
					this.blockFdmapCells(this.chooseCells[i], 1);
				}
				if(i == this.chooseCells.length-1){
					log.show('Палуба --Last');
					this.blockFdmapCells(this.chooseCells[i] + this.row);
					this.blockFdmapCells(this.chooseCells[i] + this.row, - 1);
					this.blockFdmapCells(this.chooseCells[i] + this.row, 1);
					this.blockFdmapCells(this.chooseCells[i], - 1);
					this.blockFdmapCells(this.chooseCells[i], + 1);
				}
				if(i != this.chooseCells.length-1 && i != 0){
					log.show('Палуба --Middle');
					this.blockFdmapCells(this.chooseCells[i], - 1);
					this.blockFdmapCells(this.chooseCells[i], 1);
				}				
			}

			log.show(' ***** ');
			log.show(' ');
		}

		this.chooseCells = [];
	},

	blockFdmapCells : function(idCell, shift){
		if(idCell < 0 || idCell >= this.sqRow) {
			log.show('Err блок #1');
			return false;
		}
		if(shift){
			if((idCell/this.row ^ 0) != ((idCell+shift)/this.row ^ 0)) {
				log.show('Err блок #2');
				return false;
			}
			idCell = idCell + shift;
			if(idCell < 0 || idCell >= this.sqRow){
				log.show('Err блок #3');
				return false;
			}
		}
		if(this.fdmap[idCell] == 0) {
			log.show('Выполнено');
			this.fdmap[idCell] = -1;
		}
		return true;
	},

	checkIdCell : function(idCell){
		if(idCell < 0 || idCell >= this.sqRow) return false;
		if(this.fdmap[idCell] != 0) return false;
		if(this.chooseCells.indexOf(idCell) != -1) return false;

		if(this.chooseCells.length == 1){
			var step = Math.abs(this.chooseCells[0] - idCell);
			if(step != this.row && step != 1) return false;
		}
		else if(this.chooseCells.length > 1){
			var step = Math.abs(this.chooseCells[0] - this.chooseCells[1]);
			var stepFist = Math.abs(this.chooseCells[0] - idCell);
			var stepLast = Math.abs(this.chooseCells[this.chooseCells.length-1] - idCell);
			if((stepFist != step) && (stepLast != step)) return false;
		}

		return true;	
	},

	chooseIdCell : function(idCell){
		this.chooseCells.push(idCell);
		this.chooseCells.sort(function(a, b){return a - b;});
	},

	setClickCells : function(e){
		if(!ship.countDeck) return false;
		
		var idCell = field.getCurrentIdCell(e.target);
		if(!field.checkIdCell(+idCell)) return false;
		
		field.chooseIdCell(+idCell);

		e.target.classList.remove('hover', 'good');
		e.target.classList.add('active');

		if(ship.reduceCountDeck() == 0){
			field.saveChooseCells();
		}
	},

	setMouseOverCells : function(e){
		if(!ship.countDeck) return false;

		var idCell = field.getCurrentIdCell(e.target);
		if(!field.checkIdCell(+idCell)) e.target.classList.add('hover','bad');

		e.target.classList.add('hover', 'good');
	},	

	setMouseOutCells : function(e){
		if(!ship.countDeck) return false;

		e.target.classList.remove('hover', 'good', 'bad');
	},
}

var ship = {
	numOne : 4,
	numTwo : 3,
	numThree : 2,
	numFour : 1,
	countDeck : 0,
	currentDeckShip : 0,
	currentShip : null,

	balansShips : function(deckShip){
		switch(deckShip){
			case 1: return this.numOne; break;
			case 2: return this.numTwo; break;
			case 3: return this.numThree; break;
			case 4: return this.numFour; break;
		}
	},

	reduceShips : function(deckShip){
		switch(deckShip){
			case 1: return --this.numOne; break;
			case 2: return --this.numTwo; break;
			case 3: return --this.numThree; break;
			case 4: return --this.numFour; break;
		}
	},

	chooseShip : function(el, deckShip){
		if(deckShip > 4 || deckShip < 0) return false;
		if(el.className.indexOf('active') > 0){
			this.deactivationShip(el);
			return false;
		}

		this.currentShip = el;

		var balansShip = this.balansShips(deckShip);
		if(balansShip > 0){
			this.countDeck = this.currentDeckShip = deckShip;
			this.deactivationShips();
			this.currentShip.classList.add('active');
		}	
	},


	deactivationShip : function(el){
		this.countDeck = this.currentDeckShip = 0;
		this.currentShip = null;
		el.classList.remove('active');

		//обработка массива корабля
		//раставленные точки на поле
	},	

	deactivationShips : function(){
		ships = document.getElementsByClassName('ship-row');
		for(var i=0; i < ships.length; i++){
			ships[i].classList.remove('active');
		}
	},

	reduceCountDeck : function(){
		this.countDeck--;

		if(this.countDeck == 0){
			if(this.reduceShips(this.currentDeckShip) == 0){
				this.currentShip.classList.add('disabled');
			}

			this.deactivationShip(this.currentShip);
		}
		return this.countDeck;
	},
}

window.onload = function() {
    field.init();
};