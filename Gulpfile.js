var gulp        = require('gulp');
var sass        = require('gulp-sass');
var jade        = require('gulp-jade');
var connect     = require('gulp-connect');

var paths = {
  jade:['./jade/*.jade'],
  scss:['./sass/*.scss'],
  script:['./dist/js/*.js']
};

// Static Server + watching scss/html files
gulp.task('connect', function() {
  connect.server({
    port: 1337,
    livereload: true,
    root: './dist',
  });
});

// Compile sass into CSS
gulp.task('sass', function() {
  gulp.src(paths.scss)
  .pipe(sass({outputStyle: 'compressed'}))
  .pipe(gulp.dest('./dist/css'))
  .pipe(connect.reload());
});

// Watch jade
gulp.task('jade', function() {
  gulp.src(paths.jade)
  .pipe(jade())
  .pipe(gulp.dest('./dist'))
  .pipe(connect.reload());
});
    
// Watch scripts
gulp.task('scripts', function() {
  gulp.src(paths.script)
  .pipe(connect.reload());
});

gulp.task('watcher', function() {
  gulp.watch(paths.script, ['scripts']);
  gulp.watch(paths.jade, ['jade']);
  gulp.watch(paths.scss, ['sass']);
});

gulp.task('default', ['connect', 'watcher']);